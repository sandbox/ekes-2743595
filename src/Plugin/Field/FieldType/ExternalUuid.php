<?php

namespace Drupal\external_uuid_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'field_permission_example' field type.
 *
 * @FieldType(
 *   id = "external_uuid_field",
 *   label = @Translation("External UUID"),
 *   module = "external_uuid_field",
 *   description = @Translation("Storage for a related external, non-generated, UUID."),
 *   default_widget = "external_uuid_field_widget",
 *   default_formatter = "external_uuid_field_formatter"
 * )
 */
class ExternalUuid extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    $this->setValue(array('value' => NULL), $notify);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'value' => array(
          'type' => 'varchar_ascii',
          'length' => 128,
          'binary' => FALSE,
        ),
      ),
      'unique keys' => array(
        'value_unique' => array('value'),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'max_length' => 36,
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('UUID'));

    return $properties;
  }
}

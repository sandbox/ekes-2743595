<?php

namespace Drupal\external_uuid_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Plugin implementation of a basic External UUID widget.
 *
 * @FieldWidget(
 *   id = "external_uuid_field_widget",
 *   label = @Translation("External UUID"),
 *   field_types = {
 *     "external_uuid_field"
 *   }
 * )
 */
class ExternalUuidWidget extends StringTextfieldWidget {
}

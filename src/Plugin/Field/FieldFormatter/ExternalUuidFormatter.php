<?php

namespace Drupal\external_uuid_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;

/**
 * Plugin implementation of simple UUID string formatter.
 *
 * @FieldFormatter(
 *   id = "external_uuid_field_formatter",
 *   label = @Translation("External UUID"),
 *   field_types = {
 *     "external_uuid_field",
 *   }
 * )
 */
class ExternalUuidFormatter extends StringFormatter {
}

<?php

namespace Drupal\Tests\external_uuid_field\Kernel;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

// We can use UserCreationTrait in Kernel tests as well as with Simpletests.
use Drupal\simpletest\UserCreationTrait;

/**
 * Tests External UUID field content and permissions.
 *
 * @todo requires unique test.
 *
 * @see \Drupal\KernelTests\KernelTestBase
 *
 * @group external_uuid_field
 */
class ExternalUuidFieldItemTest extends FieldKernelTestBase {

  use UserCreationTrait;

  /**
   * Modules we need.
   *
   * @var array
   */
  public static $modules = ['external_uuid_field'];

  /**
   * {@inheritdoc}
   *
   * This sets up the entity_test and user types.
   */
  protected function setUp() {
    parent::setUp();
    $type_manager = $this->container->get('entity_type.manager');

    // Set up our entity_type and user type for our new field:
    $type_manager
      ->getStorage('field_storage_config')
      ->create([
        'entity_type' => 'entity_test',
        'field_name' => 'node_external_uuid_field',
        'type' => 'external_uuid_field',
        'cardinality' => 1,
        'settings' => array(
          'max_length' => 36,
        ),
      ])->save();

    $type_manager
      ->getStorage('field_config')
      ->create([
        'entity_type' => 'entity_test',
        'field_name' => 'node_external_uuid_field',
        'bundle' => 'entity_test',
      ])->save();

    // Create a form display for the default form mode, and
    // add our field type.
    $type_manager
      ->getStorage('entity_form_display')
      ->create([
        'targetEntityType' => 'entity_test',
        'bundle' => 'entity_test',
        'mode' => 'default',
        'status' => TRUE,
      ])
      ->setComponent('node_external_uuid_field', [
        'type' => 'external_uuid_field_widget',
      ])
      ->save();

    // Now do this for the user type.
    $type_manager
      ->getStorage('field_storage_config')
      ->create([
        'field_name' => 'user_external_uuid_field',
        'entity_type' => 'user',
        'type' => 'external_uuid_field',
        'settings' => array(
          'max_length' => 36,
        ),
      ])->save();

    $type_manager
      ->getStorage('field_config')
      ->create([
        'entity_type' => 'user',
        'field_name' => 'user_external_uuid_field',
        'bundle' => 'user',
      ])->save();

    // Fetch a form display for a user. This may already exist, so check as
    // Core does.
    // @see https://api.drupal.org/api/drupal/core%21includes%21entity.inc/function/entity_get_form_display/8
    $entity_form_display
      = $type_manager
        ->getStorage('entity_form_display')
        ->load('user.user.default');
    if (empty($entity_form_display)) {
      $entity_form_display
        = $type_manager
          ->getStorage('entity_form_display')
          ->create([
            'targetEntityType' => 'user',
            'bundle' => 'user',
            'mode' => 'default',
            'status' => TRUE,
          ]);
    }
    $entity_form_display->setComponent('user_external_uuid_field', [
      'type' => 'external_uuid_field_widget',
    ])->save();
  }

  /**
   * Test entity fields of the external_uuid_field type.
   */
  public function testExternalUuidFieldItem() {
    // Verify entity creation.
    $type_manager = $this->container->get('entity_type.manager');
    $entity
      = $type_manager
        ->getStorage('entity_test')
        ->create([]);
    $value = '1234';
    $entity->node_external_uuid_field = $value;
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Verify entity has been created properly.
    $id = $entity->id();
    $entity
      = $type_manager
        ->getStorage('entity_test')
        ->load($id);

    $this->assertTrue($entity->node_external_uuid_field instanceof FieldItemListInterface, 'Field implements interface.');
    $this->assertTrue($entity->node_external_uuid_field[0] instanceof FieldItemInterface, 'Field item implements interface.');
    $this->assertEqual($entity->node_external_uuid_field->value, $value);
    $this->assertEqual($entity->node_external_uuid_field[0]->value, $value);

    // Verify changing the field's value.
    $new_value = '4321';
    $entity->node_external_uuid_field->value = $new_value;
    $this->assertEqual($entity->node_external_uuid_field->value, $new_value);

    // Read changed entity and assert changed values.
    $entity->save();

    $entity
      = $type_manager
        ->getStorage('entity_test')
        ->load($id);

    $this->assertEqual($entity->node_external_uuid_field->value, $new_value);

    $new_entity = $type_manager
      ->getStorage('entity_test')
      ->create([]);
    $new_entity->node_external_uuid_field = $new_value;
    $new_entity->name->value = $this->randomMachineName();
    $new_entity->save();

    $this->assertEqual($entity->node_external_uuid_field->value, $new_value);
  }

  /**
   * Test multiple access permissions scenarios for the field.
   */
  public function testExternalUuidFieldAccess() {

    // Let's set up some scenarios.
    $scenarios = [
      'admin_type' => [
        'perms' => ['administer the External UUID field'],
        'can_view_any' => TRUE,
        'can_edit_any' => TRUE,
      ],
      'low_access' => [
        'perms' => ['view test entity'],
        'can_view_any' => FALSE,
        'can_edit_any' => FALSE,
      ],
      'view_any' => [
        'perms' => [
          'view test entity',
          'view any External UUID field',
        ],
        'can_view_any' => TRUE,
        'can_edit_any' => FALSE,
      ],
      'edit_any' => [
        'perms' => [
          'view test entity',
          'view any External UUID field',
          'edit any External UUID field',
        ],
        'can_view_any' => TRUE,
        'can_edit_any' => TRUE,
      ],
    ];

    $value = 12345678;
    // We also need to test users as an entity to attach to.  They work
    // a little differently than most content entity types:
    $arbitrary_user = $this->createUser([], 'Some User');
    $arbitrary_user->user_external_uuid_field = (string) $value;
    $arbitrary_user->save();

    foreach ($scenarios as $name => $scenario) {
      $test_user = $this->createUser($scenario['perms'], $name);
      $entity = entity_create('entity_test');
      $entity->node_external_uuid_field = (string) ++$value;
      $entity->name->value = (string) $value;
      $entity->save();

      foreach (['can_view_any', 'can_edit_any'] as $op) {
        $this->doAccessAssertion($entity, 'node_external_uuid_field', $test_user, $name, $op, $scenario[$op]);
        $this->doAccessAssertion($arbitrary_user, 'user_external_uuid_field', $test_user, $name, $op, $scenario[$op]);
      }
    }

  }

  /**
   * Helper routine to run the assertions.
   */
  protected function doAccessAssertion($entity, $field_name, $account, $name, $op, $expected) {
    $expect_str = $expected ? "CAN" : "CANNOT";
    $assert_str = "$name $expect_str do $op on field $field_name";
    $operation = preg_match('/edit/', $op) ? "edit" : "view";
    $result = $entity->$field_name->access($operation, $account);
    if ($expected) {
      $this->assertTrue($result, $assert_str, $result . ' '  . $assert_str);
    }
    else {
      $this->assertFalse($result, $assert_str, $result . ' ' . $assert_str);
    }
  }

}
